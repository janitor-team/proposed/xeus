xeus (2.4.1-1) unstable; urgency=medium

  * New upstream version 2.4.1
  * Upload to unstable
  * Closes: #1019992

 -- Gordon Ball <gordon@chronitis.net>  Sat, 01 Oct 2022 17:45:34 +0000

xeus (2.4.0-2) experimental; urgency=medium

  [ Stephan Lachnit ]
  * Depend on cppzmq-dev instead of libzmq3-dev for zmq.hpp, and drop the
    patch required for missing cppzmq cmake files in libzmq3-dev

 -- Gordon Ball <gordon@chronitis.net>  Sat, 27 Aug 2022 21:37:35 +0200

xeus (2.4.0-1) experimental; urgency=medium

  * New upstream version 2.4.0

 -- Gordon Ball <gordon@chronitis.net>  Mon, 14 Mar 2022 22:03:36 +0000

xeus (2.3.1-1) experimental; urgency=medium

  * Drop debian/patches/0004-Keep-support-for-cppzmq-4.6.0.patch (updated in
    src:zeromq3 4.3.4-2)
  * New upstream version 2.3.1
  * Rebase patches
  * Standards-Version: 4.6.0

 -- Gordon Ball <gordon@chronitis.net>  Tue, 25 Jan 2022 19:16:10 +0000

xeus (2.1.1-1) experimental; urgency=medium

  * New upstream version 2.1.1
  * Add b-d on doctest-dev
  * Rename libxeus1 -> libxeus6 for soversion 6

 -- Gordon Ball <gordon@chronitis.net>  Wed, 13 Oct 2021 19:45:42 +0000

xeus (1.0.4-1) unstable; urgency=medium

  * d/watch: new github tarball URL
  * d/control: version dependency on xtl-dev (>= 0.7)
  * New upstream version 1.0.4
  * Refresh patches

 -- Gordon Ball <gordon@chronitis.net>  Sat, 09 Oct 2021 21:00:24 +0000

xeus (0.25.3-1) unstable; urgency=medium

  * New upstream version 0.25.3

 -- Gordon Ball <gordon@chronitis.net>  Fri, 11 Dec 2020 12:58:55 +0000

xeus (0.25.0-1) unstable; urgency=medium

  * New upstream version 0.25.0
  * Add patches:
     + Retain compatibility for cppzmq 4.6.0
     + Disable -march=native for tests to avoid warnings
  * Fix FTBFS with nocheck profile, patch from Helmut Grohne (Closes: #973982)

 -- Gordon Ball <gordon@chronitis.net>  Fri, 20 Nov 2020 20:10:24 +0000

xeus (0.24.2-2) unstable; urgency=medium

  * Source-only upload

 -- Gordon Ball <gordon@chronitis.net>  Thu, 29 Oct 2020 20:21:39 +0000

xeus (0.24.2-1) unstable; urgency=medium

  * Initial release. (Closes: #973041)

 -- Gordon Ball <gordon@chronitis.net>  Tue, 27 Oct 2020 19:44:43 +0000
